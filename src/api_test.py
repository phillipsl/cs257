#!/usr/bin/env python3
'''
    by Joyce Domogalla & Liv Phillips, based on code from Jeff Ondich
    Allows user to access list containing the names of all of the victims of
    police involved shootings in 2015 & 2016 in the United States.
    Also allows user to access demographic information for a specific victim.
'''

import argparse
import json
import urllib.request


def get_all_names():
    '''
    Returns a list containing the names of every victim, compiled by accessing
    every dictionary in the JSON list.
    '''
    url = "http://thecountedapi.com/api/counted?"

    try:
        data_from_server = urllib.request.urlopen(url).read()
        string_from_server = data_from_server.decode("utf-8")
        name_list = json.loads(string_from_server)

    except Exception as e:
        return []

    result_list = []
    for entry_dictionary in name_list:
        name = entry_dictionary["name"]
        result_list.append(name)

    return result_list

def get_specific(name):
    '''
    Returns a list containing the most relevant demographic information about
    a specific victim. The demographic info that is relevant is contained in
    list relevant_data.
    '''

    if name.endswith(" "):
        name = name[:-1]
    if name.startswith(" "):
        name = name[1:]
    name = name.replace(" ", "+")

    base_url = "http://thecountedapi.com/api/counted?name={0}"
    url = base_url.format(name)

    try:
        data_from_server = urllib.request.urlopen(url).read()
        string_from_server = data_from_server.decode("utf-8")
        name_list = json.loads(string_from_server)

    except Exception as e:
        return []

    relevant_data = ["name", "age", "sex", "race", "month", "year",
                     "state", "cause", "dept", "armed"]
    result_list = []
    if len(name_list) < 1:
        raise Exception('name not found: "{0}"'.format(name))
    if type(name) != type(""):
        raise Exception('name has wrong type: "{0}"'.format(name))
    for entry_dictionary in name_list:
        for datapt in relevant_data:
            data = str(datapt + " = " + entry_dictionary[datapt])
            result_list.append(data)

    return result_list

def main(args):
    if args.action == "list":
        all_names = get_all_names()
        for name in all_names:
            print(name)
        print("Total deaths: " + str(len(all_names)))

    elif args.action == "specific":
        specific_name = get_specific(args.name)
        for datapt in specific_name:
            print(datapt)

if __name__ == "__main__":
    '''
    Instantiates parser to help user understand and call methods.
    '''
    parser = argparse.ArgumentParser(description=
                                     "Get data on officer involved shootings \
                                     in 2015 & 2016 from The Counted API")

    parser.add_argument("action",
                        metavar="action",
                        help="enter 'list' if you want the full list of \
                        victims or 'specific' to enter a specific name",
                        choices=["list", "specific"])

    parser.add_argument("-name", help='if you entered "specific", you must also \
                                      enter "-name" followed by the name \
                                      of the victim you want to learn more \
                                      about in quotations \
                                      (example: -name "Calvin McKinnis")')

    args = parser.parse_args()
    main(args)